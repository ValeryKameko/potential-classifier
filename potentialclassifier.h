#ifndef POTENTIALCLASSIFIER_H
#define POTENTIALCLASSIFIER_H

#include <QVector>
#include <QGenericMatrix>
#include <QPointF>

using ClassificationClass = qint32;
using ClassificationFunc = std::function<ClassificationClass (const QPointF& point)>;
using EstimationFunc = std::function<double (const QPointF& point)>;
using EvaluationFunc = std::function<double (double x)>;
using ClassificationCoefficients = QGenericMatrix<2, 2, double>;

struct ClassifiedPoint {
    QPointF point;
    ClassificationClass pointClass;

    ClassifiedPoint();
    ClassifiedPoint(
            const QPointF& point,
            ClassificationClass pointClass);
};

static constexpr int CLASSES_COUNT = 2;
static const QVector<ClassificationClass> CLASSES = {-1, 1};

class PotentialClassifier
{
public:
    PotentialClassifier();

    EvaluationFunc buildEvaluationFunc();
    EstimationFunc buildEstimationFunc();
    ClassificationFunc buildClassificationFunc();

    void train(const QVector<ClassifiedPoint>& trainingPoints);
    ClassificationClass classify(const QPointF& point);
    double estimate(const QPointF& object);
    double evaluate(double x);
private:
    static ClassificationCoefficients calculateCoefficients(const ClassifiedPoint& trainingPoint);

    static double estimate(
            const ClassificationCoefficients& coefficients,
            const QPointF& point);

    static ClassificationClass classify(
            const ClassificationCoefficients& coefficients,
            const QPointF& point);

    static double evaluate(
            const ClassificationCoefficients& coefficients,
            double x);

    static double phiValue();
    static double hermiteCoefficient(int i, int j);
    static double hermiteValue(int rank, double value);

    ClassificationCoefficients coefficients;

    static constexpr int TRAINING_ITERATIONS = 10;
    static constexpr double TRAINING_LAMBDA = 0.01;
};

#endif // POTENTIALCLASSIFIER_H
