#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtMath>
#include <random>
#include <potentialclassifier.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButtonClassify_clicked();

    void on_pushButtonSetCount_clicked();

    void on_pushButtonUpdate_clicked();

    void on_pushButtonGenerate_clicked();

    void on_pushButtonTrain_clicked();

private:
    Ui::MainWindow *ui;
    std::default_random_engine generator;
    PotentialClassifier classifier;
    QVector<ClassifiedPoint> trainingPoints;

    void updateTrainingPoints();

    static constexpr double MIN_SIGMA = 1;
    static constexpr double MAX_SIGMA = 50;
    static constexpr double MIN_MU = -100;
    static constexpr double MAX_MU = 100;
};
#endif // MAINWINDOW_H
