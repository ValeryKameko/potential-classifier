#ifndef CHARTVIEW_H
#define CHARTVIEW_H

#include <QWidget>
#include <QColor>
#include <vendor/QCustomPlot/qcustomplot.h>
#include <potentialclassifier.h>

namespace Ui {
class ChartView;
}

class ChartView : public QWidget
{
    Q_OBJECT

public:
    explicit ChartView(QWidget *parent = nullptr);
    ~ChartView();

    void resetFunc(EvaluationFunc func);
    void resetData(const QVector<ClassifiedPoint> &points);

private:
    Ui::ChartView *ui;

    QMap<ClassificationClass, QCPGraph*> scatterGraphs;
    EvaluationFunc evaluationFunc;
    QCPGraph *contourGraph;

    void configurePlot(QCustomPlot* plot);

    static const QMap<ClassificationClass, QColor> COLORS;
    static constexpr int POINT_COUNT = 1000;
    static constexpr double X_MIN = -100;
    static constexpr double X_MAX = +100;
    static constexpr double Y_MIN = -100;
    static constexpr double Y_MAX = +100;

private slots:
    void replotEvaluationFunc(const QCPRange& range);
};

#endif // CHARTVIEW_H
