#include "chartview.h"
#include "ui_chartview.h"

const QMap<ClassificationClass, QColor> ChartView::COLORS = {
    {1, QColor(Qt::red)},
    {2, QColor(Qt::blue)},
};

ChartView::ChartView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ChartView)
{
    ui->setupUi(this);
    configurePlot(ui->chartView);
}

ChartView::~ChartView()
{
    delete ui;
}

void ChartView::configurePlot(QCustomPlot *plot)
{
    plot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
    plot->axisRect()->setupFullAxesBox(true);
    plot->xAxis->setLabel("x");
    plot->xAxis->setRange(X_MIN, X_MAX);
    plot->yAxis->setLabel("y");
    plot->yAxis->setRange(Y_MIN, Y_MAX);

    contourGraph = plot->addGraph();
    contourGraph->setPen(QPen(Qt::blue));

    connect(plot->xAxis, static_cast<void (QCPAxis::*)(const QCPRange&)>(&QCPAxis::rangeChanged),
            this, &ChartView::replotEvaluationFunc);

    for (ClassificationClass klass : CLASSES) {
        QCPGraph *graph = plot->addGraph();
        scatterGraphs[klass] = graph;

        QColor color = COLORS[klass];
        graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, color, color, 2));
        graph->setLineStyle(QCPGraph::lsNone);
    }
}

void ChartView::replotEvaluationFunc(const QCPRange &range)
{
    if (!evaluationFunc) return;

    QVector<double> x(POINT_COUNT), y(POINT_COUNT);
    for (int i = 0; i < POINT_COUNT; ++i) {
        x[i] = i / double(POINT_COUNT - 1) * (range.upper - range.lower) + range.lower;
        y[i] = evaluationFunc(x[i]);
    }
    contourGraph->setData(x, y);
    ui->chartView->replot();

}

void ChartView::resetFunc(EvaluationFunc func)
{
    this->evaluationFunc = func;
    replotEvaluationFunc(ui->chartView->xAxis->range());
}

void ChartView::resetData(const QVector<ClassifiedPoint> &points)
{
    QMap<ClassificationClass, QVector<double>> x, y;

    for (const auto& point : points) {
        x[point.pointClass] << point.point.x();
        y[point.pointClass] << point.point.y();
    }

    for (auto klass : CLASSES) {
        scatterGraphs[klass]->setData(x[klass], y[klass]);
    }
    ui->chartView->replot();
}
