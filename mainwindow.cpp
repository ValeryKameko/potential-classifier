#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtCharts/QScatterSeries>
#include <QtCharts/QLegendMarker>
#include <QtCore/QtMath>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->lineEditX->setValidator(new QDoubleValidator(-1000, 1000, 10));
    ui->lineEditY->setValidator(new QDoubleValidator(-1000, 1000, 10));
    ui->lineEditSetCount->setValidator(new QIntValidator(1, 1000));

    ui->tableWidgetTrainingVectors->setHorizontalHeaderLabels({"X", "Y", "Class"});
    trainingPoints << ClassifiedPoint{QPointF{}, 1};
    updateTrainingPoints();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButtonClassify_clicked()
{
    QPointF point;

    if (!ui->lineEditX->hasAcceptableInput()) {
        QMessageBox::warning(this, "Error", "X value is invalid");
        return;
    } else if (!ui->lineEditX->hasAcceptableInput()) {
        QMessageBox::warning(this, "Error", "Y value is invalid");
        return;
    }
    point.setX(ui->lineEditX->text().toDouble());
    point.setY(ui->lineEditY->text().toDouble());

    ClassificationClass pointClass = CLASSES.indexOf(classifier.classify(point)) + 1;
    QMessageBox::warning(this, "Classification", QString("Point class is %1").arg(pointClass));
}

void MainWindow::on_pushButtonSetCount_clicked()
{
    if (!ui->lineEditSetCount->hasAcceptableInput()) {
        QMessageBox::warning(this, "Error", "Set Value is invalid");
        return;
    }
    int count = ui->lineEditSetCount->text().toInt();
    trainingPoints.resize(count);

    updateTrainingPoints();
}

void MainWindow::updateTrainingPoints()
{
    if (trainingPoints.length() < ui->tableWidgetTrainingVectors->rowCount()) {
        ui->tableWidgetTrainingVectors->setRowCount(trainingPoints.length());
    }
    while (trainingPoints.length() > ui->tableWidgetTrainingVectors->rowCount()) {
        int last = ui->tableWidgetTrainingVectors->rowCount();
        const auto& trainingPoint = trainingPoints[last];
        const auto& point = trainingPoint.point;
        ui->tableWidgetTrainingVectors->insertRow(last);

        QLineEdit* lineEditX = new QLineEdit{};
        lineEditX->setValidator(new QDoubleValidator{-1000, 1000, 10});
        lineEditX->setText(QString::number(point.x()));
        ui->tableWidgetTrainingVectors->setCellWidget(last, 0, lineEditX);

        QLineEdit* lineEditY = new QLineEdit();
        lineEditY->setValidator(new QDoubleValidator{-1000, 1000, 10});
        lineEditY->setText(QString::number(point.y()));
        ui->tableWidgetTrainingVectors->setCellWidget(last, 1, lineEditY);

        QComboBox* comboBoxClass = new QComboBox{};
        comboBoxClass->addItem("1", CLASSES[0]);
        comboBoxClass->addItem("2", CLASSES[1]);
        comboBoxClass->setCurrentIndex(CLASSES.indexOf(trainingPoint.pointClass));
        ui->tableWidgetTrainingVectors->setCellWidget(last, 2, comboBoxClass);
    }

    for (int i = 0; i < trainingPoints.length(); i++) {
        auto trainingPoint = trainingPoints[i];
        auto lineEditX = static_cast<QLineEdit*>(ui->tableWidgetTrainingVectors->cellWidget(i, 0));
        auto lineEditY = static_cast<QLineEdit*>(ui->tableWidgetTrainingVectors->cellWidget(i, 1));
        auto comboBoxClass = static_cast<QComboBox*>(ui->tableWidgetTrainingVectors->cellWidget(i, 2));

        lineEditX->setText(QString::number(trainingPoint.point.x()));
        lineEditY->setText(QString::number(trainingPoint.point.y()));
        comboBoxClass->setCurrentIndex(CLASSES.indexOf(trainingPoint.pointClass));
    }
    ui->chart->resetData(trainingPoints);
}

void MainWindow::on_pushButtonUpdate_clicked()
{
    QVector<ClassifiedPoint> newTrainingPoints;

    for (int i = 0; i < ui->tableWidgetTrainingVectors->rowCount(); i++) {
        auto lineEditX = static_cast<QLineEdit*>(ui->tableWidgetTrainingVectors->cellWidget(i, 0));
        auto lineEditY = static_cast<QLineEdit*>(ui->tableWidgetTrainingVectors->cellWidget(i, 1));
        auto comboBoxClass = static_cast<QComboBox*>(ui->tableWidgetTrainingVectors->cellWidget(i, 2));
        if (!lineEditX->hasAcceptableInput()) {
            QMessageBox::warning(this, "Error", QString{"X of vector %1 value is invalid"}.arg(i));
            return;
        }
        newTrainingPoints << ClassifiedPoint{
            { lineEditX->text().toDouble(), lineEditY->text().toDouble() },
            comboBoxClass->currentData().toInt()};
    }

    trainingPoints = newTrainingPoints;
    updateTrainingPoints();
}

void MainWindow::on_pushButtonGenerate_clicked()
{
    std::uniform_real_distribution<> sigmaDistribution{MIN_SIGMA, MAX_SIGMA};
    std::uniform_real_distribution<> muDistribution{MIN_MU, MAX_MU};
    std::uniform_int_distribution<ClassificationClass> classDistribution{0, CLASSES_COUNT - 1};
    QMap<ClassificationClass, std::normal_distribution<>> distributions;
    QMap<ClassificationClass, QPointF> offsets;

    for (auto classificationClass : CLASSES) {
        double muX = muDistribution(generator);
        double muY = muDistribution(generator);
        double sigma = sigmaDistribution(generator);

        distributions[classificationClass] = std::normal_distribution<>{0, sigma};
        offsets[classificationClass] = QPointF{muX, muY};
    }

    for (auto &trainingPoint : trainingPoints) {
        ClassificationClass pointClass = CLASSES[classDistribution(generator)];
        auto& valueDistribution = distributions[pointClass];
        auto point = QPointF{ valueDistribution(generator), valueDistribution(generator) };
        trainingPoint = { offsets[pointClass] + point, pointClass };
    }

    updateTrainingPoints();
}

void MainWindow::on_pushButtonTrain_clicked()
{
    classifier.train(trainingPoints);
    ui->chart->resetFunc(classifier.buildEvaluationFunc());
}
