#include "potentialclassifier.h"
#include <QtMath>

PotentialClassifier::PotentialClassifier()
{
    coefficients.fill(0);
}

EvaluationFunc PotentialClassifier::buildEvaluationFunc()
{
    return [coefficients = this->coefficients](double x) -> double {
        return evaluate(coefficients, x);
    };
}

void PotentialClassifier::train(const QVector<ClassifiedPoint>& trainingPoints)
{
    coefficients.fill(0);

    for (int i = 0; i < TRAINING_ITERATIONS; i++) {
        for (auto& trainingPoint : trainingPoints) {
            auto coefficients = calculateCoefficients(trainingPoint);
            auto pointClass = classify(this->coefficients, trainingPoint.point);
            if (pointClass != trainingPoint.pointClass) {
                if (estimate(this->coefficients, trainingPoint.point) <= 0) {
                    this->coefficients += coefficients * TRAINING_LAMBDA;
                } else {
                    this->coefficients -= coefficients * TRAINING_LAMBDA;
                }
            }
        }
    }
}

ClassificationClass PotentialClassifier::classify(const QPointF &point)
{
    return classify(coefficients, point);
}

double PotentialClassifier::estimate(const QPointF &point)
{
    return estimate(coefficients, point);
}

double PotentialClassifier::evaluate(double x)
{
    return evaluate(coefficients, x);
}

ClassificationCoefficients PotentialClassifier::calculateCoefficients(
        const ClassifiedPoint &trainingPoint)
{
    ClassificationCoefficients coefficients;
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            coefficients(i, j) = hermiteCoefficient(i, j) *
                    hermiteValue(i, trainingPoint.point.x()) *
                    hermiteValue(j, trainingPoint.point.y());
        }
    }
    return coefficients;
}

double PotentialClassifier::estimate(
        const ClassificationCoefficients &coefficients,
        const QPointF &point)
{
    double result = 0;
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            double value = 1;
            if (i == 1) value *= point.x();
            if (j == 1) value *= point.y();
            result += coefficients(i, j) * value;
        }
    }
    return result;
}

ClassificationClass PotentialClassifier::classify(
        const ClassificationCoefficients &coefficients,
        const QPointF &point)
{
    auto value = estimate(coefficients, point);
    return ClassificationClass(copysign(-1, value));
}

double PotentialClassifier::evaluate(const ClassificationCoefficients &coefficients, double x)
{
    return - (coefficients(0, 0) + x * coefficients(1, 0)) / (coefficients(0, 1) + x * coefficients(1, 1));
}

EstimationFunc PotentialClassifier::buildEstimationFunc()
{
    return [coefficients = this->coefficients] (const QPointF &point) -> double {
        return estimate(coefficients, point);
    };
}

ClassificationFunc PotentialClassifier::buildClassificationFunc()
{
    auto valueFunc = buildEstimationFunc();
    return [valueFunc] (const QPointF &point) -> ClassificationClass {
        auto value = valueFunc(point);
        return CLASSES[value < 0 ? 0 : 1];
    };
}

double PotentialClassifier::hermiteCoefficient(int i, int j) {
    QVector<double> coefficients = {1, 2, 4};
    return coefficients[i + j];
}

double PotentialClassifier::hermiteValue(int rank, double value)
{
    double previousResult = 0;
    double currentResult = 1;
    for (int i = 1; i <= rank; i++) {
        double nextResult = 2 * value * currentResult - 2 * (i - 1) * previousResult;
        std::tie(previousResult, currentResult) = std::pair(currentResult, nextResult);
    }
    return currentResult;
}

ClassifiedPoint::ClassifiedPoint()
{
    pointClass = CLASSES[0];
}

ClassifiedPoint::ClassifiedPoint(const QPointF &point, ClassificationClass pointClass)
{
    this->point = point;
    this->pointClass = pointClass;
}
